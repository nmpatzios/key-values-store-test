package main

import (
	"encoding/json"
	"log"
	"time"

	"github.com/boltdb/bolt"
)

// Sports Typeholds the data for the sports
type Sports struct {
	ID        string    `json:"id,omitempty"`
	SportName string    `json:"sport_name,omitempty"`
	SportDesc string    `json:"sport_desc,omitempty"`
	Created   time.Time `json:"created,omitempty"`
}

//HTTPResp holds the data for json response
type HTTPResp struct {
	Status      int    `json:"status"`
	Description string `json:"description"`
	Body        string `json:"body"`
}

//KeyValueSports holds the key and and the values
type KeyValueSports struct {
	Key    string
	Sports Sports
}

//AllSports holds the data for all the key/values that are on database
type AllSports struct {
	AllSports []*KeyValueSports
}

//connect to given db
func connect() *bolt.DB {
	db, err := bolt.Open("sports.db", 0600, nil) //open or create the sports.db if not exists
	if err != nil {
		log.Fatal(err) //log the error
	}

	return db
}

//GetAllSports retun a slice with all the ke/values that exists on db by given a bucket name
func GetAllSports(bucket string) []*KeyValueSports {
	db := connect()                   //trying to connect to db
	defer db.Close()                  //if no errors close the connection
	var s []*KeyValueSports           //copy of a slice that point to struct
	db.View(func(tx *bolt.Tx) error { //using bolt package to read all the key/values
		c := tx.Bucket([]byte(bucket))

		c.ForEach(func(k, v []byte) error { //read its key/value and return an erros
			var sport *Sports                //copy Sport struct
			err := json.Unmarshal(v, &sport) //convert json into struct typr sport
			if err != nil {                  //check for errors when trying to convert
				println(err.Error())
			}

			r := &KeyValueSports{Key: string(k), Sports: *sport} //initialize the struct by given the corect key for values per key
			s = append(s, r)                                     //append each key/values to  slice

			return nil // nil errors
		})
		return nil //nil errors
	})
	return s //return the slice
}
