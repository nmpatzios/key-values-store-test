# in order to make the application run you will need to execute the foollow commands before build it:

1. Install GB Go Builder by typing: 
go get github.com/constabulary/gb/...
website: https://getgb.io/

2. cd path to the project
3. fetch all the golang packages that are necessary to execute the app by typing: in this case the vendor folder exists with the 2 packages so you can escape and go to step 4

    a. gb vendor fetch github.com/gorilla/mux //package for routing
    b. gb vendor fetch github.com/boltdb/bolt //packge for storing key/values to datastore db

4. install nodejs, npm ,gulp

5. type gulp and hit enter

6. open a browser and type localhost:8000

p.s.
I used visual studio code to create the app if you want to build or run the app using visual studio code  builder

you have to include to .vscode folder the follow text
1. to launch.json file 
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Launch",
            "type": "go",
            "request": "launch",
            "mode": "debug",
            "remotePath": "",
            "port": 2345,
            "host": "127.0.0.1",
            "program": "${workspaceRoot}/src/server",
            "env": {
                "GOPATH": "${workspaceRoot}:${workspaceRoot}/vendor"
            },
            "args": [],
            "showLog": true
        }
    ]
}

2. to settings.json file
{
    "go.gopath": "${workspaceRoot}:${workspaceRoot}/vendor",
    "vsicons.presets.angular": false
}

