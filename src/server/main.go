package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/boltdb/bolt"
	"github.com/gorilla/mux"
)

var indexTemplate = template.Must(template.New("sports.html").ParseFiles("views/sports.html")) //parse the html template

//index render the index page
func index(w http.ResponseWriter, r *http.Request) {
	all := GetAllSports("sports") //get all key/values from the sports.db for the given bucket
	var results AllSports         //create a copy for the stuct
	results.AllSports = all       //put all key/values to a struct

	indexTemplate.Execute(w, results) //render the the html page with struct data
}

//AddNewSport to create a sport record to database
func AddNewSport(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body) // decoder the for the r
	var sport *Sports
	err := decoder.Decode(&sport) //decode the sports struct with the json values from the boby
	if err != nil {               //check for errors
		print(err.Error())
	}
	t := time.Now()                             //get the time
	t.Format("01/02/2006")                      //format the time
	sport.Created = t                           //put the formatted time to stuct value
	s1 := rand.NewSource(time.Now().UnixNano()) //create unique random number
	r1 := rand.New(s1)                          //create unique random number

	sport.ID = strconv.Itoa(r1.Intn(100)) //create unique random number from 1 - 100 and put into struct value

	db := connect()                           //connect with the database
	defer db.Close()                          //close the database
	err = db.Update(func(tx *bolt.Tx) error { //update the database by suing bolt package
		b, err := tx.CreateBucketIfNotExists([]byte("sports")) //check if the bucket exists if no the create
		if err != nil {                                        //check for erroes when trying to create
			return fmt.Errorf("create bucket: %s", err.Error())
		}
		encoded, err := json.Marshal(sport) //encoding to json the struct values
		if err != nil {

			json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "could not encode Person " + sport.ID}) //return to body  status,description, the id if there is error
			return fmt.Errorf("Could not encode Sport %s: %s", sport.ID, err.Error())

		}

		return b.Put([]byte(sport.ID), encoded) //insert to databse the key,the values for the key
	})
	if err != nil { //chek for error when trying to insert to to database
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: err.Error()}) //return to body json data the  status and the error
	}
	json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "Successfully Inserted Sport Into the Database", Body: (sport.ID)}) //return to body json data if no erros
}

func main() {

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static")))) //path to static files jquery etc.

	router := mux.NewRouter()                                            //initialize a new router using gorilla package
	router.HandleFunc("/", index)                                        //path to index page with data
	router.HandleFunc("/api/insert-people", AddNewSport).Methods("POST") //get the json data from given url and parse it to the AddNewSPort func when post from form

	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
